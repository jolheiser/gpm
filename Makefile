GO ?= go
VERSION ?= $(shell git describe --tags --always | sed 's/-/+/' | sed 's/^v//')

.PHONY: build
build:
	$(GO) build -ldflags '-s -w -X "go.jolheiser.com/gpm/router.Version=$(VERSION)"'

.PHONY: lint
lint:
	earth +lint-cli
	earth +lint-lib

.PHONY: fmt
fmt: fmt-cli fmt-lib

.PHONY: test
test: test-cli test-lib

.PHONY: fmt-cli
fmt-cli:
	$(GO) fmt ./...

.PHONY: test-cli
test-cli:
	$(GO) test -race ./...

.PHONY: fmt-lib
fmt-lib:
	@cd go-gpm && $(GO) fmt ./...

.PHONY: test-lib
test-lib:
	@cd go-gpm && $(GO) test -race ./...

.PHONY: docker-build
docker-build:
	docker build -f docker/Dockerfile -t jolheiser/gpm .

.PHONY: docker-push
docker-push: docker-build
	docker push jolheiser/gpm