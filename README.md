# GPM

Go Package Mapping

### Motivation

I personally use a lot of nice libraries in Go, but every time I start a new project I have to go hunting for import paths again!

Enter GPM, a glorified mapping of simple names to go-get imports.

For example, I use [urfave/cli](https://github.com/urfave/cli) for all of my CLI projects. I've used it enough times to remember the import path, but let's say I didn't.  
Using either a GPM server or local config, I can instead `gpm get cli` which finds `cli` in my map and runs `go get github.com/urfave/cli/v2`.

### Commands

* `add`    - Add a local package
* `remove` - Remove a local package
* `list`   - List local packages
* `config` - Change local configuration
* `get` - Get a list of packages
  * e.g. `gpm get beaver survey bbolt cli chi` to get all the modules needed for gpm itself (assuming the map resolves to the same packages)
* `server` - Start a gpm server


### Server

gpm will call out to a gpm server to find a package.  
This makes it much simpler to have a central library of packages rather than exporting and importing between environments.

Want to run your own server? It's very easy! This CLI comes packaged with the server inside, simply run `gpm server` to start up a GPM server.  
Remember to set a `--token`!  
Put it behind your favorite reverse proxy, and it's ready to go!


## License

[MIT](LICENSE)