package cmd

import (
	"context"
	"regexp"
	"strings"

	"go.jolheiser.com/gpm/cmd/flags"
	"go.jolheiser.com/gpm/database"
	"go.jolheiser.com/gpm/go-gpm"

	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var Add = cli.Command{
	Name:    "add",
	Aliases: []string{"a"},
	Usage:   "Add a package",
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name:        "force",
			Aliases:     []string{"f"},
			Usage:       "Overwrite existing package without prompt",
			Destination: &flags.Force,
		},
		&cli.BoolFlag{
			Name:        "local",
			Aliases:     []string{"l"},
			Usage:       "local mode",
			Destination: &flags.Local,
		},
	},
	Before: localOrToken,
	Action: doAdd,
}

var vPattern = regexp.MustCompile(`v\d+$`)

func doAdd(_ *cli.Context) error {
	goGetQuestion := &survey.Input{
		Message: "Package go-get import",
	}
	var goGetAnswer string

	if err := survey.AskOne(goGetQuestion, &goGetAnswer); err != nil {
		return err
	}

	goGet := strings.Split(goGetAnswer, "/")
	defaultName := goGet[len(goGet)-1]
	// Check if go-get is actually pointing to a versioned module
	if vPattern.MatchString(defaultName) {
		defaultName = goGet[len(goGet)-2]
	}

	nameQuestion := &survey.Input{
		Message: "Package name for gpm",
		Default: defaultName,
	}
	var nameAnswer string

	if err := survey.AskOne(nameQuestion, &nameAnswer); err != nil {
		return err
	}

	pkg := gpm.Package{
		Name:   nameAnswer,
		Import: goGetAnswer,
	}

	if flags.Local {
		db, err := database.Load(flags.Database)
		if err != nil {
			return err
		}
		if err := db.PutPackage(pkg); err != nil {
			return err
		}
	} else {
		client := gpm.New(flags.Token, gpm.WithServer(flags.Server))
		if err := client.Add(context.Background(), pkg); err != nil {
			return err
		}
	}

	beaver.Infof("Added %s", yellow.Format(nameAnswer))
	return nil
}
