package cmd

import (
	"context"
	"errors"
	"os"
	"path/filepath"

	"go.jolheiser.com/gpm/cmd/flags"
	"go.jolheiser.com/gpm/database"
	"go.jolheiser.com/gpm/go-gpm"
	"go.jolheiser.com/gpm/router"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver/color"
)

var yellow = color.FgYellow

func New() *cli.App {
	app := cli.NewApp()
	app.Name = "gpm"
	app.Usage = "Go Package Manager"
	app.Version = router.Version
	app.Commands = []*cli.Command{
		&Add,
		&Get,
		&List,
		&Remove,
		&Search,
		&Server,
		&Update,
	}
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "server",
			Aliases:     []string{"s"},
			Usage:       "gpm server to use",
			Value:       gpm.DefaultServer,
			EnvVars:     []string{"GPM_SERVER"},
			Destination: &flags.Server,
		},
		&cli.StringFlag{
			Name:        "token",
			Aliases:     []string{"t"},
			Usage:       "gpm auth token to use",
			DefaultText: "${GPM_TOKEN}",
			EnvVars:     []string{"GPM_TOKEN"},
			Destination: &flags.Token,
		},
		&cli.StringFlag{
			Name:        "database",
			Aliases:     []string{"d"},
			Usage:       "path to gpm database for server",
			Value:       dbPath(),
			DefaultText: "`${HOME}/gpm.db` or `${BINPATH}/gpm.db`",
			EnvVars:     []string{"GPM_DATABASE"},
			Destination: &flags.Database,
		},
	}
	return app
}

func dbPath() string {
	fn := "gpm.db"
	home, err := os.UserHomeDir()
	if err != nil {
		bin, err := os.Executable()
		if err != nil {
			return fn
		}
		return filepath.Join(filepath.Dir(bin), fn)
	}
	return filepath.Join(home, fn)
}

func localOrToken(_ *cli.Context) error {
	if flags.Local && flags.Token == "" {
		return errors.New("server interaaction requires --token")
	}
	return nil
}

func listPackages() ([]gpm.Package, error) {
	var pkgs []gpm.Package
	if flags.Local {
		db, err := database.Load(flags.Database)
		if err != nil {
			return pkgs, err
		}
		pkgs, err = db.Packages()
		if err != nil {
			return pkgs, err
		}
	} else {
		client := gpm.New(flags.Token, gpm.WithServer(flags.Server))
		info, err := client.Info(context.Background())
		if err != nil {
			return pkgs, err
		}
		pkgs = info.Packages
	}
	return pkgs, nil
}
