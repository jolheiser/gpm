package flags

var (
	Server   string
	Token    string
	Database string

	Local bool
	Force bool
	Port  int
)
