package cmd

import (
	"context"
	"os"
	"os/exec"
	"strings"

	"go.jolheiser.com/gpm/cmd/flags"
	"go.jolheiser.com/gpm/go-gpm"

	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var Get = cli.Command{
	Name:    "get",
	Aliases: []string{"g"},
	Usage:   "Get package(s)",
	Action:  doGet,
}

func doGet(ctx *cli.Context) error {
	pkgs := ctx.Args().Slice()
	if len(pkgs) == 0 {
		pkgsQuestion := &survey.Multiline{
			Message: "Enter packages to get, one for each line",
		}
		var pkgsAnswer string

		if err := survey.AskOne(pkgsQuestion, &pkgsAnswer); err != nil {
			return err
		}

		pkgs = strings.Split(pkgsAnswer, "\n")
	}

	client := gpm.New(flags.Token, gpm.WithServer(flags.Server))
	for _, p := range pkgs {
		pkg, err := client.Get(context.Background(), p)
		if err != nil {
			beaver.Error(err)
			continue
		}

		beaver.Infof("getting `%s`...", pkg)
		if err := goGet(pkg.Import); err != nil {
			beaver.Error(err)
		}
	}

	return nil
}

func goGet(url string) error {
	cmd := exec.Command("go", "get", url)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
