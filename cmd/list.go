package cmd

import (
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/urfave/cli/v2"
)

var List = cli.Command{
	Name:    "list",
	Aliases: []string{"ls", "l"},
	Usage:   "List local packages",
	Action:  doList,
}

func doList(_ *cli.Context) error {
	pkgs, err := listPackages()
	if err != nil {
		return err
	}
	w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
	for _, pkg := range pkgs {
		s := fmt.Sprintf("%s\t%s\n", pkg.Name, pkg.Import)
		_, _ = w.Write([]byte(s))
	}
	return w.Flush()
}
