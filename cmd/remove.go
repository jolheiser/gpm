package cmd

import (
	"context"

	"go.jolheiser.com/gpm/cmd/flags"
	"go.jolheiser.com/gpm/database"
	"go.jolheiser.com/gpm/go-gpm"

	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var Remove = cli.Command{
	Name:    "remove",
	Aliases: []string{"rm"},
	Usage:   "Remove package(s)",
	Before:  localOrToken,
	Action:  doRemove,
}

func doRemove(_ *cli.Context) error {
	pkgs, err := listPackages()
	if err != nil {
		return err
	}

	pkgSlice := make([]string, len(pkgs))
	pkgMap := make(map[string]gpm.Package)
	for idx, pkg := range pkgs {
		pkgSlice[idx] = pkg.Name
		pkgMap[pkg.Name] = pkg
	}

	pkgQuestion := &survey.Select{
		Message: "Select package to remove",
		Options: pkgSlice,
	}

	var pkgName string
	if err := survey.AskOne(pkgQuestion, &pkgName); err != nil {
		return err
	}

	pkg := gpm.Package{
		Name:   pkgName,
		Import: pkgMap[pkgName].Import,
	}

	if flags.Local {
		db, err := database.Load(flags.Database)
		if err != nil {
			return err
		}
		if err := db.RemovePackage(pkg.Name); err != nil {
			return err
		}
	} else {
		client := gpm.New(flags.Token, gpm.WithServer(flags.Server))
		if err := client.Remove(context.Background(), pkg); err != nil {
			return err
		}
	}

	beaver.Infof("Removed %s", yellow.Format(pkgName))
	return nil
}
