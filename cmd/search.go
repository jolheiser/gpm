package cmd

import (
	"go.jolheiser.com/gpm/go-gpm"

	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var Search = cli.Command{
	Name:    "search",
	Aliases: []string{"s"},
	Usage:   "Search packages",
	Action:  doSearch,
}

func doSearch(_ *cli.Context) error {
	pkgs, err := listPackages()
	if err != nil {
		return err
	}

	pkgSlice := make([]string, len(pkgs))
	pkgMap := make(map[string]gpm.Package)
	for idx, pkg := range pkgs {
		pkgSlice[idx] = pkg.Name
		pkgMap[pkg.Name] = pkg
	}

	q := &survey.MultiSelect{
		Message: "Select packages",
		Options: pkgSlice,
	}

	var a []string
	if err := survey.AskOne(q, &a); err != nil {
		return err
	}

	for _, name := range a {
		pkg, ok := pkgMap[name]
		if !ok {
			beaver.Errorf("could not find package for `%s`", name)
			continue
		}
		beaver.Infof("getting `%s`...", name)
		if err := goGet(pkg.Import); err != nil {
			beaver.Error(err)
		}
	}

	return nil
}
