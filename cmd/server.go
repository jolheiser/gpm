package cmd

import (
	"errors"
	"fmt"
	"net/http"

	"go.jolheiser.com/gpm/cmd/flags"
	"go.jolheiser.com/gpm/database"
	"go.jolheiser.com/gpm/router"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var Server = cli.Command{
	Name:    "server",
	Aliases: []string{"web"},
	Usage:   "Start the gpm server",
	Flags: []cli.Flag{
		&cli.IntFlag{
			Name:        "port",
			Aliases:     []string{"p"},
			Usage:       "Port to run the gpm server on",
			Value:       3333,
			EnvVars:     []string{"GPM_PORT"},
			Destination: &flags.Port,
		},
	},
	Action: doServer,
}

func doServer(_ *cli.Context) error {
	if flags.Token == "" {
		return errors.New("gpm server requires --token")
	}

	db, err := database.Load(flags.Database)
	if err != nil {
		beaver.Fatalf("could not load database at %s: %v", flags.Database, err)
	}

	beaver.Infof("Running gpm server at http://localhost:%d", flags.Port)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", flags.Port), router.New(flags.Token, db)); err != nil {
		return err
	}
	return nil
}
