package cmd

import (
	"context"

	"go.jolheiser.com/gpm/cmd/flags"
	"go.jolheiser.com/gpm/database"
	"go.jolheiser.com/gpm/go-gpm"

	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var Update = cli.Command{
	Name:    "update",
	Aliases: []string{"u"},
	Usage:   "Update a package",
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name:        "local",
			Aliases:     []string{"l"},
			Usage:       "local mode",
			Destination: &flags.Local,
		},
	},
	Before: localOrToken,
	Action: doUpdate,
}

func doUpdate(_ *cli.Context) error {
	pkgs, err := listPackages()
	if err != nil {
		return err
	}

	pkgSlice := make([]string, len(pkgs))
	pkgMap := make(map[string]gpm.Package)
	for idx, pkg := range pkgs {
		pkgSlice[idx] = pkg.Name
		pkgMap[pkg.Name] = pkg
	}

	pkgQuestion := &survey.Select{
		Message: "Select package to update",
		Options: pkgSlice,
	}

	var pkgName string
	if err := survey.AskOne(pkgQuestion, &pkgName); err != nil {
		return err
	}

	importQuestion := &survey.Input{
		Message: "New import path",
		Default: pkgMap[pkgName].Import,
	}

	var importPath string
	if err := survey.AskOne(importQuestion, &importPath); err != nil {
		return err
	}

	pkg := gpm.Package{
		Name:   pkgName,
		Import: importPath,
	}

	if flags.Local {
		db, err := database.Load(flags.Database)
		if err != nil {
			return err
		}
		if err := db.PutPackage(pkg); err != nil {
			return err
		}
	} else {
		client := gpm.New(flags.Token, gpm.WithServer(flags.Server))
		if err := client.Update(context.Background(), pkg); err != nil {
			return err
		}
	}

	beaver.Infof("Updated %s", yellow.Format(pkgName))
	return nil
}
