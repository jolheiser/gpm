package database

import (
	"os"
	"path/filepath"
	"testing"

	"go.jolheiser.com/gpm/go-gpm"
)

var db *Database

func TestMain(m *testing.M) {
	tmp, err := os.MkdirTemp(os.TempDir(), "gpm")
	if err != nil {
		panic(err)
	}
	dbPath := filepath.Join(tmp, "gpm.db")

	db, err = Load(dbPath)
	if err != nil {
		panic(err)
	}

	code := m.Run()

	// Cleanup
	if err := os.RemoveAll(tmp); err != nil {
		panic(err)
	}

	os.Exit(code)
}

func TestPackage(t *testing.T) {

	// Does not exist
	_, err := db.Package("test")
	if err == nil {
		t.Log("test package should not exist")
		t.FailNow()
	}

	// Add
	pkg := gpm.Package{
		Name:   "test",
		Import: "gitea.com/test/testing",
	}
	err = db.PutPackage(pkg)
	if err != nil {
		t.Logf("could not put test package: %v\n", err)
		t.FailNow()
	}

	// Update
	pkg.Import = "gitea.com/testing/test"
	err = db.PutPackage(pkg)
	if err != nil {
		t.Logf("could not put test package: %v\n", err)
		t.FailNow()
	}

	// Check
	p, err := db.Package("test")
	if err != nil {
		t.Logf("should find test package: %v\n", err)
		t.FailNow()
	}
	if p.Import != pkg.Import {
		t.Logf("test package did not match update:\n\texpected: %s\n\t     got: %s\n", pkg.Import, p.Import)
		t.FailNow()
	}

	// Remove
	err = db.RemovePackage("test")
	if err != nil {
		t.Log("could not remove test package")
		t.FailNow()
	}

	// Check
	_, err = db.Package("test")
	if err == nil {
		t.Log("test package should not exist after being removed")
		t.FailNow()
	}
}
