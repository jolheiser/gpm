//+build docs

package main

import (
	"os"
	"strings"

	"go.jolheiser.com/gpm/cmd"
)

func main() {
	app := cmd.New()

	md, err := app.ToMarkdown()
	if err != nil {
		panic(err)
	}

	// FIXME Why is this not fixed yet??
	md = md[strings.Index(md, "#"):]

	fi, err := os.Create("DOCS.md")
	if err != nil {
		panic(err)
	}
	if _, err := fi.WriteString(md); err != nil {
		panic(err)
	}
	if err := fi.Close(); err != nil {
		panic(err)
	}
}
