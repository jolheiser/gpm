package main

import (
	"os"

	"go.jolheiser.com/gpm/cmd"

	"go.jolheiser.com/beaver"
)

func main() {
	if err := cmd.New().Run(os.Args); err != nil {
		beaver.Error(err)
	}
}
