package router

import (
	"context"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"go.jolheiser.com/gpm/database"
	"go.jolheiser.com/gpm/go-gpm"

	"go.jolheiser.com/beaver"
)

var (
	server *httptest.Server
	token  = "TestingRouter"
)

// NOTE: The router test is more or less a copy/paste from go-gpm
// However, this ensures that testing is the same with the "real" router and DB
func TestMain(m *testing.M) {
	tmp, err := os.MkdirTemp(os.TempDir(), "gpm")
	if err != nil {
		panic(err)
	}
	dbPath := filepath.Join(tmp, "gpm.db")

	db, err := database.Load(dbPath)
	if err != nil {
		beaver.Fatalf("could not load database at %s: %v", dbPath, err)
	}

	server = httptest.NewServer(New(token, db))

	code := m.Run()

	// Cleanup
	if err := os.RemoveAll(tmp); err != nil {
		panic(err)
	}

	os.Exit(code)
}

func TestRouter(t *testing.T) {
	ctx := context.Background()
	client := gpm.New("", gpm.WithServer(server.URL))

	// Info
	checkInfo(t, client, 0)

	pkg1 := gpm.Package{
		Name:   "test1",
		Import: "gitea.com/test/testing",
	}
	pkg2 := gpm.Package{
		Name:   "test2",
		Import: "gitea.com/testing/test",
	}

	// Add (without token)
	if err := client.Add(ctx, pkg1); err == nil {
		t.Log("adding without token should fail")
		t.Fail()
	}

	// Add (with token)
	client = gpm.New(token, gpm.WithServer(server.URL))
	checkAdd(t, client, pkg1, pkg2)

	// Info (after second package)
	checkInfo(t, client, 2)

	// Check package
	checkGet(t, client, pkg2)

	// Update package
	checkUpdate(t, client, pkg1)

	// Remove
	checkRemove(t, client, pkg1)

	// Info (final)
	checkInfo(t, client, 1)
}

func checkInfo(t *testing.T, client *gpm.Client, numPackages int) {
	info, err := client.Info(context.Background())
	if err != nil {
		t.Logf("info should not return error: %v\n", err)
		t.Fail()
	}
	if info.Version != Version || info.NumPackages != numPackages {
		t.Log("info did not match expected")
		t.Fail()
	}
}

func checkGet(t *testing.T, client *gpm.Client, pkg gpm.Package) {
	ctx := context.Background()
	_, err := client.Get(ctx, "test3")
	if err == nil {
		t.Log("should not be able to get invalid package")
		t.Fail()
	}

	// Check valid package
	p, err := client.Get(ctx, "test2")
	if err != nil {
		t.Logf("should not be able to get invalid package: %v\n", err)
		t.Fail()
	}
	if p != pkg {
		t.Log("valid package should match pkg")
		t.Fail()
	}
}

func checkAdd(t *testing.T, client *gpm.Client, pkg1, pkg2 gpm.Package) {
	ctx := context.Background()
	if err := client.Add(ctx, pkg1); err != nil {
		t.Logf("pkg1 should be added: %v\n", err)
		t.Fail()
	}
	if err := client.Add(ctx, pkg2); err != nil {
		t.Logf("pkg2 should be added: %v\n", err)
		t.Fail()
	}
	// Duplicate package
	if err := client.Add(ctx, pkg1); err == nil {
		t.Log("pkg1 should already exist")
		t.Fail()
	}
}

func checkUpdate(t *testing.T, client *gpm.Client, pkg gpm.Package) {
	ctx := context.Background()
	// Update invalid package
	if err := client.Update(ctx, gpm.Package{Name: "test4", Import: "gitea.com/invalid"}); err == nil {
		t.Log("should not be able to update invalid package")
		t.Fail()
	}

	// Update valid package
	pkg.Import = "gitea.com/tester/testing"
	if err := client.Update(ctx, pkg); err != nil {
		t.Logf("should be able to update valid package: %v\n", err)
		t.Fail()
	}
}

func checkRemove(t *testing.T, client *gpm.Client, pkg gpm.Package) {
	ctx := context.Background()
	if err := client.Remove(ctx, pkg); err != nil {
		t.Logf("should be able to remove package: %v\n", err)
		t.Fail()
	}

	// Remove (idempotent)
	if err := client.Remove(ctx, pkg); err != nil {
		t.Logf("should be able to remove package idempotently: %v\n", err)
		t.Fail()
	}
}
